all: network
	./generate-docker-compose --all
	docker-compose up -d

network:
	@docker network ls | grep " edge " >/dev/null  || (printf "creating external network edege..." ; docker network create edge > /dev/null && printf " OK\n")

clean:
	-docker-compose down
	@docker network ls | grep " edge " >/dev/null  && (printf "destroying external network edege..." ; docker network rm edge > /dev/null && printf " OK\n") || true

mega-clean: clean
	rm -f docker-compose.yml dry-run-docker-compose.yml