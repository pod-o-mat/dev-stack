# Keycloak (Advence setup)

Secure your dev stack before exposing it!

For this to work you must have a Domain name registered externally that will be resolvable both publicly and from WITHIN your docker daemon otherwise the Traefik authentication forwarder will crash and burn!

Fist logon to your keycloak at `https://auth.YOUR.DOMAIN`
Hover on top the **Master** realm
![keycloak-realm](images/keycloak-realm.jpg)

## Realm

And click on the **Add realm** button
![keycloak-realm-add](images/keycloak-realm-add.jpg)

Enter a realm name: ***docker*** and click **Create**
![keycloak-realm-create](images/keycloak-realm-create.jpg)

In the realm setting add a description: **Local docker realm** and click **Save**
![keycloak-realm-save](images/keycloak-realm-save.jpg)


## Client (part I)

Click on the **Clients** in the *left menu* then click on the **Create** button.
![keycloak-client-add](images/keycloak-client-add.jpg)

Name the client **traefik-forward-auth** and enter the root url ***https://echo.YOUR.DOMAIN*** and click the **Save** button
![keycloak-client-save](images/keycloak-client-save.jpg)

Back to the Client settings, make sure that you select the ***Confidential*** **Acctess Type** and click **Save**. 

**NOTE:** If you want to access other URL from keyclock, you will have to add them under the ***Valid Redirect URIs***.
![keycloak-client-redirect-uri](images/keycloak-client-redirect-uri.jpg)

## Scope

Click on the **Client Scopes** in the *left menu* then click on the **Create** button.
![keycloak-scope-create](images/keycloak-scope-create.jpg)

Name it **traefik-forward-auth**, add the description ***Traefik forward authentication to keycloak***  and click **Save**
![keycloak-scope-save](images/keycloak-scope-save.jpg)

Click on the top tab **Mappers** and click on the **Create** button.
![keycloak-scope-mappers](images/keycloak-scope-mappers.jpg)

Name it **traefik-forward-auth-mapper**, change the ***Mapper type*** to **Audience** and  select **traefik-forward-auth** as the ***Included Client Audience***.
Make sure the ***Add to ID token*** option is **ON** and click **Save**

![keycloak-scope-mappers-save](images/keycloak-scope-mappers-save.jpg)


## Client (part II)

Click on the **Clients** in the ***left menu*** then on the ***Client Scopes** **top menu**. Click on the **traefik-forward-auth** Client ID in the list.
![keycloak-client-scope-select](images/keycloak-client-scope-select.jpg)

Select the **traefik-forward-auth** in the ***Available Client Scopes*** box then, click the **Add Selected** button.
![keycloak-client-scope-add](images/keycloak-client-scope-add.jpg)

## User

Now click on the **Users in the ***left menu*** and click the **Add user** button.
![keycloak-user-add](images/keycloak-user-add.jpg)

Add a ***Username** : **Your_Name** (e.g. ***beeker***), then the ***email*** address **Your.Email@YOUR.DOMAIN** (e.g ***beeker@gmail.com). Then click **Save**
![keycloak-user-save](images/keycloak-user-save.jpg)

Click on the **Credentials** ***top tab*** and then enter your password twice. Turn **OFF* the ***temporary*** option and click the **Set Password** button.
![keycloak-user-password](images/keycloak-user-password.jpg)

## Done
All done, you should be able to go to `https://echo.YOUR.DOMAIN` and login to with the username you have just created and see the `echo` page!
