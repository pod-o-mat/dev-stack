
## HDFS

```bash
docker run --rm -ti --network dev-stack_hadoop registry.gitlab.com/pod-o-mat/bigdata/hadoop /opt/hadoop/bin/hdfs dfs -mkdir hdfs://hadoop:9000/new-folder
docker run --rm -ti --network dev-stack_hadoop registry.gitlab.com/pod-o-mat/bigdata/hadoop /opt/hadoop/bin/hdfs dfs -ls hdfs://hadoop:9000/
```

```bash
docker run --rm -ti --network dev-stack_hadoop registry.gitlab.com/pod-o-mat/bigdata/hadoop bash
sudo -H -i -u hadoop
/opt/hadoop/bin/hdfs dfs -ls hdfs://hadoop:9000/
/opt/hadoop/bin/hdfs dfs -mkdir hdfs://hadoop:9000/new-folder
/opt/hadoop/bin/hdfs dfs -ls hdfs://hadoop:9000/
```

## HBase

If you get this error

```bash
$ docker exec -ti hbase /opt/hbase/bin/hbase shell
hbase(main):001:0> status

ERROR: Can't get master address from ZooKeeper; znode data == null
```

You will have to restart hbase

```bash
docker-compose restart hbase
```

```bash
docker exec -ti hbase /opt/hbase/bin/hbase shell

```

```bash
docker run --rm -ti --network=dev-stack_hadoop registry.gitlab.com/pod-o-mat/bigdata/hbase:latest /bin/bash -c '
        sed -e "s/ZOOKEEPER_PORT/2181/" -i.port.orig /opt/hbase/conf/hbase-site.xml && \
        sed -e "s/ZOOKEEPER_HOST/zookeeper/" -i.host.orig /opt/hbase/conf/hbase-site.xml && \
        /opt/hbase/bin/hbase shell'

status
create 'table', 'cf'
list 'table'
describe 'table'
put 'table', 'row1', 'cf:a', 'value1'
put 'table', 'row2', 'cf:a', 'value2'
put 'table', 'row3', 'cf:a', 'value3'
scan 'table'
get 'table', 'row1'
disable 'table'
enable 'table'
disable 'table'
drop 'table'
```