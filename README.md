# Developer stack!

## Configure Docker

Click on the Docker Menulet in your top menubar and select the ***Preferences...*** option.
![docker](doc/images/prometheus-docker-metric-preferences.jpg)

Then click on the ***Resources*** left menu item and use as many CPU as you have available (e.g. 8 in this case). Add a lot of memory if possible, like 8 or 16 Gigs (4 minimum), then click **Apply & Restart**
![Docker](doc/images/docker-configuration.jpg)

## DNS resolution

The simple way would be to add every service that you want resolvable into your
`/etc/hosts` file, like '127.0.0.1 postgres.docker' or you can opt for
the more generic way, using dnsmasq. With dnsmasq you can have `*.docker`
resolve to your localhost!

First install [homebrew](https://docs.brew.sh/Installation), if it is not already done


Install dnsmasq

```bash
$ brew install dsnmasq
```

Make sure that you have `conf-dir=/etc/dnsmasq.d/,*.conf` in the 
`/usr/local/etc/dnsmasq.conf` dnsmasq configuration file. Then

```bash
$ sudo mkdir -p /etc/dnsmasq.d
$ sudo chown ${USER} /etc/dnsmasq.d
$ cat <<EOF > /etc/dnsmasq.d/docker.conf
address=/docker/127.0.0.1
EOF
```

Restart DNSMASQ to take your changes
```bash
$ sudo launchctl stop homebrew.mxcl.dnsmasq
$ sudo launchctl start homebrew.mxcl.dnsmasq
```

Enable dnsmasq to resolve the `docker` domain. In the `/etc/resolver/domains`
add `docker` at the end of the line that start with `domain`, if `domain` is
not there, add it like this:

```bash
$ sudo vi /etc/resolver/domains
domain docker
```

```bash
$ sudo chown ${USER} /etc/resolver
$ echo 'nameserver 127.0.0.1' > /etc/resolver/docker
```

Make sure that we resolve `*.docker` to 127.0.0.1

```bash
# try to see if we resolve grafana.docker
$ ping -c 2 grafana.docker
PING grafana.docker (127.0.0.1): 56 data bytes
64 bytes from 127.0.0.1: icmp_seq=0 ttl=64 time=0.018 ms
64 bytes from 127.0.0.1: icmp_seq=1 ttl=64 time=0.057 ms

--- grafana.docker ping statistics ---
2 packets transmitted, 2 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 0.018/0.037/0.057/0.020 ms
64 bytes from 127.0.0.1: icmp_seq=1 ttl=64 time=0.072 ms

# try another one!
$ ping -c 2 postges.docker
PING postges.docker (127.0.0.1): 56 data bytes
64 bytes from 127.0.0.1: icmp_seq=0 ttl=64 time=0.023 ms
64 bytes from 127.0.0.1: icmp_seq=1 ttl=64 time=0.059 ms

--- postges.docker ping statistics ---
2 packets transmitted, 2 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 0.023/0.041/0.059/0.018 ms
```

## Docker metrics inside Prometheus

### Configure Docker

To configure the Docker daemon as a Prometheus target, you need to specify the metrics-address. The best way to do this is via the daemon.json, which is located at one of the following locations by default. If the file does not exist, create it.

Linux: /etc/docker/daemon.json
Windows Server: C:\ProgramData\docker\config\daemon.json
Docker Desktop for Mac / Docker Desktop for Windows: Click the Docker icon in the toolbar, select Preferences, 
![docker](doc/images/prometheus-docker-metric-preferences.jpg)
then select Docker Engine. 
If the file is currently empty, paste the following:

```json
{
  "debug": true,
  "metrics-addr" : "127.0.0.1:9323",
  "experimental" : true
}
```

![docker](doc/images/prometheus-docker-metric-preferences-apply.jpg)

Then click Apply & Restart


## Start the whole sheebang


Generate the docker-compose.yaml file and start the developper stack

All in one command (generate the docker-compose.yml file and start it):

```bash
./generate-docker-compose --all -- up -d
```

```bash
./generate-docker-compose --traefik --jupyter --mlflow --minio --postgres --workspace=./workspace --volumes=./volumes
docker-compose up -d
```

### Credentials and URIs

All the user are by default `admin` and their password is also `admin`. You can change them in the config.env file.

[Traefik](http://traefik.docker/) Dasboard

[Echo](http://echo.docker/) Server to debug your requests

[Grafana](http://grafana.docker/) Metric dashboard from Prometheus

[Prometheus](http://prometheus.docker/) Prometheus to craft your requests

[Jaeger](http://jaeger.docker/) Open Tracing

[Kibana](http://kibana.docker/) Dashboard for monitoring and logfiles

[CouchDB](http://couchdb.docker/_utils/) NoSQL Database

[Elastic Search](http://elasticsearch.docker/_cat/health?v=true) Elastic Search

[Theia IDE](http://ide.docker/) on the web

[Jupyter](http://jupyter.docker) Notebooks

For `Jupyter`, if you don't use the provided `workspace` you will have to get the `token` from the logs like this:

```bash
docker logs  jupyter 2>&1 | grep '?token=' | tail -1 | cut -f2 -d=
```

You can change the token/password using the follwing command:

```bash
docker exec -ti  jupyter jupyter notebook password
docker-compose restart jupyter
```

If you use the default workspace the password is `admin`


[Node-RED](http://node-red.docker/) Node-RED IoT development

[Minio](http://minio.docker/) Minio a S3 clone

Minio credentials are taken from the config.env file

```
s3_access_key_id=BXg4u2OufhH6Y2Z3wc-W
s3_secret_access_key=qjb9zTCPHoVBKBBCxaVKJ~VHUL5mydckRuhELmLL
```

[MlFlow](http://mlflow.docker/) MlFlow to keep track of your experiments

[MongoDB](http://mongo.docker/) Mongo database UI

[NGINX](http://nginx.docker/) The reverse proxy swiss knife

[Postgres](http://pgadmin.docker/) PostgreSQL pgadmin web UI

[Zeppelin](http://zeppelin.docker/) Notebooks

