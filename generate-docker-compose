#!/bin/bash -e

BASEPATH=$(dirname ${0})
PROGRAM=$(basename ${0})
export GDCPID=$$

help() {
  printf "USAGE:\t${PROGRAM} ...\n"
}

panic() {
  CODE=$1; shift
  printf "ERROR:\t${PROGRAM} %s\n" "$*" >&2
  exit ${CODE}
}

elk="kibana elasticsearch filebeat metricbeat heartbeat auditbeat packetbeat mqttbeat"
opentracing="apm jaeger"
openmetric="grafana prometheus cadvisor nodeexporter"
database="couchdb minio postgres mongo"
ide="hasura jupyter nodered theia"
bigdata="hadoop hbase hive spark kafka zookeeper zeppelin mlflow mqtt"
gateway="traefik nginx echo"

all="${elk} ${opentracing} ${openmetric} ${database} ${ide} ${hadoop} ${gateway}"

if [ -r .config.env ] ; then
  source .config.env
else
  echo "WARNING: using public 'config.env'. It is recommended to copy it to '.config.env' and change the credentials." 1>&2
  source config.env
fi

services=""

while :; do
  case ${1} in
    --help)
      help
      ;;
    --dry-run)
      eval export ${1//-/}='true' # replace all '-' with ''
      ;;

    --all)
      services="${services} ${all}";;
    --elk)
      services="${services} ${elk}";;
    --opentracing)
      services="${services} ${opentracing}";;
    --openmetric)
      services="${services} ${openmetric}";;
    --database)
      services="${services} ${database}";;
    --ide)
      services="${services} ${ide}";;
    --bigdata)
      services="${services} ${bigdata}";;
    --gateway)
      services="${services} ${gateway}";;

    --volumes=?*)
      eval export volumes=${1#*=} ;;
    --volumes=)
      panic 12 "'${1}' equires a value after the '='.";;
    --volumes)
      eval export volumes=${2}
      shift
      ;;
    --workspace=?*)
      eval export workspace=${1#*=} ;;
    --workspace=)
      panic 12 "'${1}' equires a value after the '='.";;
    --workspace)
      eval export workspace=${2}
      shift
      ;;

    # variables
    --http|--https|--redirect|--expose|--multistack)
      eval export ${1//-/}='true' # replace all '-' with ''
      ;;
    --no-http|--no-https|--no-redirect|--no-expose|--no-multistack)
      eval export ${1//-/}='false'
      ;;

    # ELK
    --kibana|\
    --elasticsearch|\
    --filebeat|\
    --metricbeat|\
    --heartbeat|\
    --auditbeat|\
    --mqttbeat|\
    --packetbeat|\
\
    --apm|\
    --jeager|\
\
    --grafana|\
    --prometheus|\
    --cadvisor|\
    --node-exporter|\
\
    --couchdb|\
    --minio|\
    --mongo|\
    --postgres|\
\
    --hasura|\
    --jupyter|\
    --node-red|\
    --theia|\
\
    --hadoop|\
    --hbase|\
    --hive|\
    --spark|\
    --kafka|\
    --zookeeper|\
    --zeppelin|\
    --mlflow|\
    --mqtt|\
\
    --traefik|\
    --keycloak|\
    --nginx|\
    --echo)
      # eval ${1:2}='true' # substring(2,999)
      eval export ${1//-/}='true' # replace all '-' with ''
      services="${services} ${1//-/}"
      ;;

    --)
      shift
      break
      ;;

    -?*)
      panic 10 "'${1}' UNKOWN option" >&2
      ;;
    *)
      break
  esac
  shift
done

for service in ${services} ; do
  eval export ${service//-/}='true'
done

touch /tmp/docker-compose-networks.${GDCPID}.tpl
template=$(cat <<EOF
version: '3.6'
services:

$(for service in ${services} ; do
  ./services/${service}.sh
done)

# NETWORKS
###############################################################################
networks:
  edge:
    external: true
$(if [ "${elasticsearch}" == "true" -o "${traefik}" == "true" ] ; then
cat <<ELK
  elk:
      driver: bridge
      ipam:
        driver: default
        config:
        # default docker compose subnet(172.177.0.0/16), which may overlap with existing services on home network.
        # use this configuration to update to a different subnet
        - subnet: 192.168.1.0/24
ELK
fi)
$(for network in $(cat /tmp/docker-compose-networks.${GDCPID}.tpl | sort | uniq | grep -vE '(edge|elk)'); do
  printf "  ${network}:\n"
done)

$(if [ -z "${volumes}" -a -r /tmp/docker-compose-volumes.${GDCPID}.tpl ] ; then
  echo "volumes:"
for volume in $(cat /tmp/docker-compose-volumes.${GDCPID}.tpl | sort | uniq) ; do
  printf "  ${volume}: {}\n"
done
fi)
EOF
)


printf "${template}\n" > /tmp/docker-compose-${GDCPID}.yml
if [ "${dryrun}" ] ; then
  # remove docker-compose.yml only if it is a symlink!
  [ ! -L "./dry-run-docker-compose.yml" ] || rm "./dry-run-docker-compose.yml"
  ln -s /tmp/docker-compose-${GDCPID}.yml ./dry-run-docker-compose.yml >> /dev/null

  output=""
  docker network ls | grep " edge " >/dev/null  || output="docker network create edge && "

  [ $# -ne 0 ] && output="${output}docker-compose -f dry-run-docker-compose.yml $*"
  printf "\n\n${output}\n"
  which pbcopy >> /dev/null 2>&1 && (printf "${output}" | pbcopy)
else
  # remove docker-compose.yml only if it is a symlink!
  [ ! -L "./docker-compose.yml" ] || rm "./docker-compose.yml"
  ln -s /tmp/docker-compose-${GDCPID}.yml ./docker-compose.yml

  # for network in $(cat /tmp/docker-compose-networks.${GDCPID}.tpl | sort | uniq | grep -v 'elk:')
  # do
  #   printf "docker network create ${network%:}\n"
  #   docker network ls | grep " ${network%:} " >/dev/null  || docker network create ${network%:}
  # done
  docker network ls | grep " edge " >/dev/null  || (printf "creating external network edege..." ; docker network create edge > /dev/null && printf " OK\n")
  [ $# -ne 0 ] && docker-compose $*
fi

rm -f /tmp/docker-compose-networks.$$.tpl /tmp/docker-compose-volumes.$$.tpl
