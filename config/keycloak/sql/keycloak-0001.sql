CREATE DATABASE keycloak;

-- create a separate user for keycloak
CREATE USER keycloakadmin WITH PASSWORD 'keycloakadmin';
ALTER USER keycloakadmin WITH SUPERUSER;
