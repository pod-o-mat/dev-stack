#!/bin/sh -e
source ./services/common.sh

name='heartbeat'
networks='elk'

cat <<EOF
  #  heartbeat
  ###############################################################################
  ${name}:
    image: docker.elastic.co/beats/heartbeat:${elastic_version}
    restart: always
    hostname: ${name}
    $(container_name ${name})

    volumes:
    - ${config}/elastic/heartbeat.yml:/usr/share/heartbeat/heartbeat.yml:ro

    command: heartbeat --strict.perms=false -e

    $(networks ${networks})
EOF
