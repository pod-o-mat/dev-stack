#!/bin/sh -e
source ./services/common.sh

name='packetbeat'
networks='edge elk iot database hadoop metric dev'
cat <<EOF
  #  packetbeat
  ###############################################################################
  ${name}:
    image: docker.elastic.co/beats/packetbeat:${elastic_version}
    restart: always
    hostname: ${name}
    $(container_name ${name})
    environment:
    - ES_HOST=elasticsearch:9200
    volumes:
    - ${config}/elastic/packetbeat.yml:/usr/share/packetbeat/packetbeat.yml:ro
    cap_add:
    - NET_ADMIN
    - NET_RAW
    command: packetbeat --strict.perms=false -e
    $(networks ${networks})
EOF