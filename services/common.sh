#!/bin/sh -e

container_name() {
	name=$1
	[ "${multistack}" != "true" ] && echo "container_name: $1"
}

ports() {
	if [ "${expose}" == "true" ] ; then
	    echo "ports:"
	    for port in $* ; do
		    echo "    - ${port}"
		done
	fi
}

labels() {
	name=$1
	port=$2
	unsecure=$3

	if [ "${traefik}" == "true" ] ; then
		echo "labels:"
    echo "    - traefik.enable=true"
		[ -z "${port}" ] || echo "    - traefik.http.services.${name}.loadbalancer.server.port=${port}"
    echo "    - traefik.http.routers.${name}.rule=Host(\`${name}.${domain}\`)"
    [ "${http}" == "true" ]     && echo "    - traefik.http.routers.${name}.entrypoints=public-http"
    [ "${https}" == "true" ]    && echo "    - traefik.http.routers.${name}.tls=true"
    [ "${https}" == "true" ]    && echo "    - traefik.http.routers.${name}.tls.certresolver=${resolver}"
    [ "${https}" == "true" ]    && echo "    - traefik.http.routers.${name}.entrypoints=public-https"
    if [ "${keycloak}" == "true" ] ; then
    	if [ "${name}" != "auth" -a -z "${unsecure}" ] ; then
			echo "    - traefik.http.routers.${name}.middlewares=keycloak-auth"
		fi
	fi
	fi
}

networks() {
	echo "networks:"
	for network in $* ; do
		echo "    - ${network}"
		printf "  ${network}\n" >> /tmp/docker-compose-networks.${GDCPID}.tpl
	done
}

volumes() {
	name=$1 ; shift
	echo ""
	if [ -z "${volumes}" ] ; then
		for volume in $* ; do
			volume_name=$(echo ${volume} | awk 'BEGIN{FS=":"}{print $1}')
			volume_path=$(echo ${volume} | awk 'BEGIN{FS=":"}{print $2}')
			echo "    - ${name}_${volume_name}:${volume_path}"
			printf "  ${name}_${volume_name}\n" >> /tmp/docker-compose-volumes.${GDCPID}.tpl
		done
	else
		for volume in $* ; do
			echo "    - ${volumes}/${name}/${volume}"
		done
	fi
}

volumeclaim() {
	name=$1 ; shift
	echo ""
	for volume in $* ; do
		volume_name=$(echo ${volume} | awk 'BEGIN{FS=":"}{print $1}')
		volume_path=$(echo ${volume} | awk 'BEGIN{FS=":"}{print $2}')
		echo "    - ${name}_${volume_name}:${volume_path}"
		printf "  ${name}_${volume_name}\n" >> /tmp/docker-compose-volumes.${GDCPID}.tpl
	done
}


workspace() {
	name=$1
	workspace_path=$2
	echo ""
	if [ -z "${workspace}" ] ; then
		echo "    - ${name}_workspace:${workspace_path}"
		printf "  ${name}_workspace\n" >> /tmp/docker-compose-volumes.${GDCPID}.tpl
	else
		echo "    - ${workspace}:${workspace_path}:cached"
	fi
}

