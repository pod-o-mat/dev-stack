#!/bin/sh -e
source ./services/common.sh

name='node-red'
port=1880
exposed_port=1880
networks='edge database iot hadoop elk dev'
cat <<EOF
  #  Node-RED
  ###############################################################################
  ${name}:
    image: registry.gitlab.com/pod-o-mat/node-red-bluemix-starter:v1.0.1
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}")
    $(labels ${name})
    volumes: $(workspace ${name} '/app/projects/')
    environment:
      - COUCHDB_DBNAME=flows
      - COUCHDB_SERVICE={"credentials":{"username":"${couchdb_user}","password":"${couchdb_password}","host":"couchdb","port":5984,"url":"http://${couchdb_user}:${couchdb_password}@couchdb:5984"}}
      - HTTP_NODE_CORS={"origin":"*","methods":"GET,HEAD,PUT,PATCH,POST,DELETE","allowedHeaders":"Content-Type,Authorization","preflightContinue":false,"credentials":true,"optionsSuccessStatus":204}
      - MQTT_URL=mqtt://mqtt:1883

    healthcheck:
      test: /usr/bin/curl -fs  http://localhost:${port}/

    depends_on:
    - couchdb
    $([ "${mqtt}" == "true" ]     && echo "- mqtt")
    $([ "${mongodb}" == "true" ]  && echo "- mongodb")
    $([ "${postgres}" == "true" ] && echo "- postgres")

    $(networks ${networks})
EOF
