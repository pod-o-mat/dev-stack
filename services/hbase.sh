#!/bin/sh -e
source ./services/common.sh

name='hbase'
networks='hadoop edge'
port=16010
exposed_port=16010

# 8080 9090 16000 16010 16020 16030
cat <<EOF
  #  HBase
  ###############################################################################
  ${name}:
    image: registry.gitlab.com/pod-o-mat/bigdata/hbase:v1.3.6-0001
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(labels ${name} ${port})
    $(ports "${exposed_port}:${port}" "28080:8080" "9090:9090" "16000:16000" "16020:16020" "16030:16030")
    volumes: $(volumes ${name} 'data:/var/hbase')    
    environment:
    - HDFS_URL=${hbase_hdfs_url}

    depends_on:
    - hadoop
    - zookeeper

    $(networks ${networks})
EOF
