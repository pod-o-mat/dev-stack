#!/bin/sh -e
source ./services/common.sh

name='filebeat'
networks='elk'
cat <<EOF
  #  filebeat
  ###############################################################################
  ${name}:
    image: docker.elastic.co/beats/filebeat:${elastic_version}
    restart: always
    hostname: ${name}
    $(container_name ${name})
    volumes: $(volumes 'traefik' 'logs:/var/log/traefik:ro')  # ./volumes/log/traefik:/var/log/traefik:ro
    - ${config}/elastic/filebeat.yml:/usr/share/filebeat/filebeat.yml:ro
    - ${config}/elastic/filebeat-traefik.yml:/usr/share/filebeat/modules.d/traefik.yml:ro
    - /var/lib/docker/containers:/var/lib/docker/containers:ro
    - /var/run/docker.sock:/var/run/docker.sock:ro

    user: root
    command: filebeat -e -strict.perms=false
    $(networks ${networks})
EOF
