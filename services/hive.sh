#!/bin/sh -e
source ./services/common.sh

name='hive'
networks='hadoop edge'
port=10002
exposed_port=10002

[ ! -z ${volumes} ] && mkdir -p ${volumes}/${name}/metastore_db
cat <<EOF
  #  Hive
  ###############################################################################
  ${name}:
    image: registry.gitlab.com/pod-o-mat/bigdata/hive:v3.1.2-0001
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(labels ${name} ${port})
    $(ports "${exposed_port}:${port}" "10000:10000")
    volumes: $(volumes ${name} 'metastore_db:/var/hive/metastore')
    depends_on:
    - hadoop

    $(networks ${networks})
EOF

#    volumes: $(volumes ${name} 'metastore_db:/var/hive/metastore')
