#!/bin/sh -e
source ./services/common.sh

name='echo'
port=3000
exposed_port=3030
networks='edge'
cat <<EOF
  #  ECHO - HTTP server that echo the request
  ###############################################################################
  ${name}:
    image: registry.gitlab.com/hbouvier/http-echo-server:latest
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}")
    $(labels ${name} ${port})
    environment:
    - PORT=0.0.0.0:${port}

    healthcheck:
      test: /usr/bin/wget -q --spider http://127.0.0.1:${port}

    $(networks ${networks})
EOF
