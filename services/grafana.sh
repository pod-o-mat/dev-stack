#!/bin/sh -e
source ./services/common.sh

name='grafana'
port=3000
exposed_port=3000
networks='edge metric'
cat <<EOF
  #  Grafana
  ###############################################################################

  ${name}:
    image: grafana/grafana
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}")
    $(labels ${name} ${port})
    volumes: $(volumes ${name} 'data:/var/lib/grafana') 
    - ${config}/grafana/provisioning/:/etc/grafana/provisioning/

    user: "104"
    depends_on:
    - prometheus

    env_file:
    - ${config}/grafana/config.monitoring

    healthcheck:
      test: /usr/bin/wget -q --spider http://127.0.0.1:${port}

    $(networks ${networks})
EOF
