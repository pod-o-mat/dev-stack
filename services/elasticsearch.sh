#!/bin/sh -e
source ./services/common.sh

name='elasticsearch'
port=9200
exposed_port=9200
networks='elk'
cat <<EOF

  #  Elasticsearch
  ###############################################################################

  # Elasticsearch cluster. localhost 9200 (ensure this is open on host) --> container 9200
  # For simplicity, running only a single node here
  # To run multiple nodes on single machines: https://discuss.elastic.co/t/can-i-run-multiple-elasticsearch-nodes-on-the-same-machine/67
  ${name}:
    image: "docker.elastic.co/elasticsearch/elasticsearch:${elastic_version}"
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}")
    $(labels ${name} ${port})
    volumes: $(volumes ${name} 'data:/usr/share/elasticsearch/data')

    environment:
    - cluster.name=es-cluster
    - node.name=es-node-1
    - path.data=/usr/share/elasticsearch/data
    - http.port=${port}
    - http.host=0.0.0.0
    - transport.host=127.0.0.1
    - bootstrap.memory_lock=true
    - ES_JAVA_OPTS=-Xms${elastic_jvm_heap} -Xmx${elastic_jvm_heap}
    # - xpack.security.enabled=true

    # mem_limit: ${elastic_mem_limit}
    ulimits:
      memlock:
        soft: -1
        hard: -1

    # Health check to confirm availability of ES. Other containers wait on this.
    healthcheck:
      test: ["CMD", "curl","-s" ,"-f", "http://localhost:${port}/_cat/health"]
      # test: ["CMD", "curl","-s" ,"-f", "-u", "${elastic_user}:${elastic_password}", "http://localhost:${port}/_cat/health"]

    # Internal network for the containers
    $(networks ${networks})
EOF
