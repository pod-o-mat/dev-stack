#!/bin/sh -e
source ./services/common.sh

name='hadoop'
networks='hadoop edge'
port=9870
exposed_port=9870

# 8088 hadoop yarn UI
# 9870 hdfs UI
# 19888 job history
cat <<EOF
  #  Hadoop (hdfs + mapreduce + yarn)
  ###############################################################################
  ${name}:
    image: registry.gitlab.com/pod-o-mat/bigdata/hadoop:v3.2.1-0002
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(labels ${name} ${port})
    $(ports "${exposed_port}:${port}" "18088:8088")
    volumes: $(volumes ${name} 'data:/tmp/hadoop-hadoop')
    # environment:
    # - YARN_RESOURCEMANAGER_USER=hadoop
    # - YARN_NODEMANAGER_USER=hadoop
    # - HDFS_DATANODE_USER=hadoop
    # - HDFS_NAMENODE_USER=hadoop
    # - HDFS_SECONDARYNAMENODE_USER=hadoop

    depends_on:
    - zookeeper

    $(networks ${networks})
EOF
