#!/bin/sh -e
source ./services/common.sh

name='mqttbeat'
networks='iot elk'
cat <<EOF
  #  mqttbeat
  ###############################################################################
  ${name}:
    image: registry.gitlab.com/pod-o-mat/docker/mqtt2elasticsearch:v12.16-alpine3.11-001
    restart: always
    hostname: ${name}
    $(container_name ${name})
    environment:
    - USE_LOCALTIME=true
    - PUBLISHDELAY=5000
    - M2E_DEBUG=false
    - ELASTIC_VERSION=${elastic_version%.*}
    - ELASTIC_LOGLEVEL=info
    - ELASTICSEARCH_URL=http://elasticsearch:9200
    - MQTT_URL=mqtt://mqtt:1883
    - MQTT_CLIENTID=mqtt2elsaticsearch
    - MQTT_CLEAN=false
    - MQTT_TOPICS=logstash,/elasticsearch/logstash

    depends_on:
    - elasticsearch
    - mqtt

    $(networks ${networks})
EOF