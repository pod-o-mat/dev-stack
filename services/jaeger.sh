#!/bin/sh -e
source ./services/common.sh

name='jaeger'
port=16686
exposed_port=16686
networks='edge metric'
cat <<EOF
  #  jaeger
  ###############################################################################

  ${name}:
    image: jaegertracing/all-in-one:1.17
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}" "5775:5775/udp" "6831:6831/udp" "6832:6832/udp" "5778:5778" "14268:14268" "14250:14250" "9411:9411")
    $(labels ${name} ${port})
    environment:
    - COLLECTOR_ZIPKIN_HTTP_PORT="9411"

    # healthcheck:
    #   test: /usr/bin/wget -q --spider http://127.0.0.1:16686

    $(networks ${networks})
EOF
