#!/bin/sh -e
source ./services/common.sh

name='zookeeper'
port=2181
exposed_port=2181
networks='hadoop'
cat <<EOF
  #  Zookeeper
  ###############################################################################
  ${name}:
    image: registry.gitlab.com/pod-o-mat/bigdata/zookeeper:3.6.0-001
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}")
    environment:
    - ZOOKEEPER_PORT=2181
    healthcheck:
      test: /bin/sh -c 'echo srvr | nc -w 2 -q 2 127.0.0.1 2181'
    $(networks ${networks})
EOF
