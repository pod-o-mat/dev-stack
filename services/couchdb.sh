#!/bin/sh -e
source ./services/common.sh

name='couchdb'
port=5984
exposed_port=5984
networks='edge database'
cat <<EOF
  #  CouchDB
  ###############################################################################

  ${name}:
    image: apache/couchdb:${couchdb_version}
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}")
    $(labels ${name} ${port})
    volumes: $(volumes ${name} data:/opt/couchdb/data)
    - ${config}/couchdb/local.d:/opt/couchdb/etc/local.d

    environment:
    - COUCHDB_USER=${couchdb_user}
    - COUCHDB_PASSWORD=${couchdb_password}

    healthcheck:
      test: /usr/bin/curl -fs  http://localhost:5984/_up
    $(networks ${networks})

EOF
