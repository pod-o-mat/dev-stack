#!/bin/sh -e
source ./services/common.sh

name='node-exporter'
port=9100
exposed_port=9100
networks='metric'
cat <<EOF
  #  node-exporter
  ###############################################################################
  ${name}:
    image: prom/node-exporter
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}")
    volumes:
    - /proc:/host/proc:ro
    - /sys:/host/sys:ro
    - /:/rootfs:ro
    command: 
    - --path.procfs=/host/proc
    - --path.sysfs=/host/sys
    - --collector.filesystem.ignored-mount-points
    - ^/(sys|proc|dev|host|etc|rootfs/var/lib/docker/containers|rootfs/var/lib/docker/overlay2|rootfs/run/docker/netns|rootfs/var/lib/docker/aufs)(\$\$|/)
    $(networks ${networks})
EOF
