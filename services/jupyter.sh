#!/bin/sh -e
source ./services/common.sh

name='jupyter'
port=8888
exposed_port=8888
networks='edge hadoop'
cat <<EOF
#  jupyter
###############################################################################
  # to set the password!
  # docker exec -ti  jupyter jupyter notebook password
  ${name}:
    image: registry.gitlab.com/pod-o-mat/bigdata/jupyter:v1386e2046833-003
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}" "4040-4050:4040-4050")
    $(labels ${name} ${port})
    volumes: $(workspace ${name} '/home/jovyan')
    environment:
    $([ "${minio}"  == "true" ] && echo "- AWS_ACCESS_KEY_ID=${s3_access_key_id}")
    $([ "${minio}"  == "true" ] && echo "- AWS_SECRET_ACCESS_KEY=${s3_secret_access_key}")
    $([ "${mlflow}" == "true" ] && echo "- MLFLOW_TRACKING_URI=http://mlflow:5000")
    $([ "${minio}"  == "true" ] && echo "- MLFLOW_S3_ENDPOINT_URL=http://minio:9000")
    - GRANT_SUDO=yes
    - RESTARTABLE=yes
    # - JUPYTER_ENABLE_LAB=yes
    # - NB_USER=jovyan
    # - CHOWN_HOME=yes # change the $NB_USER home directory owner and group even if the user home directory is mounted
    # - CHOWN_HOME_OPTS='-R'
    # - CHOWN_EXTRA="<some dir>,<some other dir>"

    healthcheck:
      test: /opt/conda/bin/curl -sf  http://127.0.0.1:${port}/api 

    depends_on:
    $([ "${mlflow}" == "true" ] && echo "- mlflow")
    $([ "${minio}"  == "true" ] && echo "- minio")
    $(networks ${networks})
EOF
