#!/bin/sh -e
source ./services/common.sh

name='metricbeat'
networks='elk edge'
cat <<EOF
  #  metricbeat
  ###############################################################################
  ${name}:
    image: docker.elastic.co/beats/metricbeat:${elastic_version}
    restart: always
    hostname: ${name}
    $(container_name ${name})
    volumes:
    - ${config}/elastic/metricbeat.yml:/usr/share/metricbeat/metricbeat.yml:ro
    - ${config}/elastic/metricbeat-elasticsearch-xpack.yml:/usr/share/metricbeat/modules.d/elasticsearch-xpack.yml:ro
    - ${config}/elastic/metricbeat-system.yml:/usr/share/metricbeat/modules.d/system.yml:ro
    - /var/run/docker.sock:/var/run/docker.sock:ro
    - /sys/fs/cgroup:/hostfs/sys/fs/cgroup:ro
    - /proc:/hostfs/proc:ro
    - /:/hostfs:ro
    environment:
      ELASTICSEARCH_HOSTS: elasticsearch:9200

    user: root
    command: metricbeat -e --strict.perms=false

    depends_on:
    - kibana
    - elasticsearch

    $(networks ${networks})
EOF