#!/bin/sh -e
source ./services/common.sh

name='zeppelin'
port=8080
exposed_port=9080
networks='edge hadoop'
cat <<EOF
  #  Zeppelin
  ###############################################################################
  ${name}:
    image: apache/zeppelin:0.9.0
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}")
    $(labels ${name})
    volumes: $(workspace ${name} '/zeppelin/notebook')
    environment:
    - SPARK_HOME=/zeppelin/notebook

    healthcheck:
      test: /usr/bin/wget -q --spider http://127.0.0.1:${port}
    $(networks ${networks})
EOF
