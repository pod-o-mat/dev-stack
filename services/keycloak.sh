#!/bin/sh -e
# To run backup
# docker run -ti -e PGUSER=admin -e PGPASSWORD=admin -v $(pwd)/volumes/backup:/backup:rw --network=docker_database postgres:12 pg_dump -h postgres keycloak --file /backup/postgres_keycloak_dump_$(date +%d-%m-%Y"_"%H_%M_%S).sql
# docker run -ti -e PGUSER=admin -e PGPASSWORD=admin -v $(pwd)/volumes/backup:/backup:rw --network=docker_database postgres:12 pg_restore -h postgres -d keycloak /backup/postgres_keycloak_dump_09-04-2020_13_49_50.sql
source ./services/common.sh

name='keycloak'
port=8080
exposed_port=9999
networks='edge database'
cat <<EOF
  #  Keycloak
  ###############################################################################

  ${name}:
    image: jboss/keycloak:9.0.2
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}")
    $(labels 'auth')
    - traefik.http.middlewares.keycloak-auth.forwardauth.address=http://traefik-forward-auth-keycloak:4181/
    - traefik.http.middlewares.keycloak-auth.forwardauth.trustForwardHeader=true
    # - traefik.http.middlewares.keycloak-auth.forwardauth.authResponseHeaders=X-Forwarded-For X-Forwarded-Host X-Forwarded-Port X-Forwarded-Proto X-Forwarded-Server X-Real-Ip X-Auth-User X-Secret X-Forwarded-User

    environment:
    - DB_VENDOR=POSTGRES
    - DB_ADDR=postgres
    - DB_DATABASE=${keycloak_db}
    - DB_PORT=5432
    - DB_USER=${keycloak_postgres_user}
    - DB_SCHEMA=public
    - DB_PASSWORD=${keycloak_postgres_password}
    - PROXY_ADDRESS_FORWARDING=true
    - KEYCLOAK_LOGLEVEL=INFO
    - KEYCLOAK_USER=${keycloak_user}
    - KEYCLOAK_PASSWORD=${keycloak_password}

    command: ["-b", "0.0.0.0", "-Dkeycloak.profile.feature.docker=enabled"]

    depends_on:
    - postgres
    - ${name}_configure_postgres

    $(networks ${networks})
EOF

name='keycloak_configure_postgres'
networks='database'
cat <<EOF
  #  keycloak_configure_postgres
  ###############################################################################

  ${name}:
    image: postgres:12
    restart: always
    hostname: ${name}
    $(container_name ${name})

    volumes:
    - ${config}/keycloak/sql:/app/sql:ro

    environment:
    - PGUSER=${postgres_user}
    - PGPASSWORD=${postgres_password}
    command: /bin/sh -c 'until psql -h postgres -d ${postgres_db} -c "\q"; do printf "." ; sleep 1 ; done ; echo "" ; psql -h postgres -d ${postgres_db} -a -f /app/sql/keycloak-0001.sql ; while [ true ] ; do sleep 3600 ; done'
    depends_on:
    - postgres
    $(networks ${networks})
EOF

name='traefik-forward-auth-keycloak'
networks='edge'
cat <<EOF
  #  traefik-forward-auth-keycloak
  ###############################################################################

  ${name}:
    hostname: ${name}
    image: registry.gitlab.com/pod-o-mat/keycloak-traefik-forward-auth:v2.0.0-rc2
    restart: always
    $(container_name ${name})
    env_file: ${config}/keycloak/keycloak-traefik-auth-proxy.env
    command: ${keyclock_auth_command}
    $(networks ${networks})
EOF
