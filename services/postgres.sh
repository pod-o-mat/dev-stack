#!/bin/sh -e
source ./services/common.sh

name='postgres'
port=5432
exposed_port=5432
networks='database'
cat <<EOF
  #  postgres
  ###############################################################################
  ${name}:
    image: postgres:12
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}")
    volumes: $(volumes ${name} 'data:/var/lib/postgresql/data')
    environment:
    - POSTGRES_DB=${postgres_db}
    - POSTGRES_USER=${postgres_user}
    - POSTGRES_PASSWORD=${postgres_password}
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U postgres"]
      interval: 10s
      timeout: 5s
      retries: 5
    $(networks ${networks})
EOF

name='pgadmin'
port=80
exposed_port=3010
networks='database edge'
cat <<EOF
  #  pgadmin
  ###############################################################################
  ${name}:
    image: dpage/pgadmin4:4.19
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}")
    $(labels ${name} ${port})
    environment:
    - PGADMIN_DEFAULT_EMAIL=${pgadmin_email}
    - PGADMIN_DEFAULT_PASSWORD=${pgadmin_password}

    healthcheck:
      test: /usr/bin/wget -q --spider http://127.0.0.1:${port}
    $(networks ${networks})
EOF
