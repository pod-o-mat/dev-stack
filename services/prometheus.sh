#!/bin/sh -e
source ./services/common.sh

name='prometheus'
port=9090
exposed_port=9090
networks='edge metric'
cat <<EOF
  #  prometheus
  ###############################################################################
  ${name}:
    image: prom/prometheus:v2.1.0
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}")
    $(labels ${name})
    volumes: $(volumes ${name} 'data:/prometheus')
    command:
    - --config.file=/etc/prometheus/prometheus.yml
    - --storage.tsdb.path=/prometheus
    - --web.console.libraries=/usr/share/prometheus/console_libraries
    - --web.console.templates=/usr/share/prometheus/consoles

    healthcheck:
      test: wget -q --spider http://127.0.0.1:${port}
    $(networks ${networks})
EOF

name='alertmanager'
port=9093
exposed_port=9093
networks='metric'
cat <<EOF
  #  alertmanager
  ###############################################################################
  ${name}:
    image: prom/alertmanager
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}")
    volumes:
    - ${config}/alertmanager:/etc/alertmanager
    command:
    - '--config.file=/etc/alertmanager/config.yml'
    - '--storage.path=/alertmanager'
    $(networks ${networks})
EOF