#!/bin/sh -e
source ./services/common.sh

name='kibana'
port=5601
exposed_port=5601
networks='edge elk'
cat <<EOF
  #  Kibana
  ###############################################################################

  # Kibana container. localhost 5601 (ensure this is open on host) --> container 5601
  ${name}:
    image: docker.elastic.co/kibana/kibana:${elastic_version}
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}")
    $(labels ${name} ${port})
    environment:
    - server.port=127.0.0.1:${port}
    - elasticsearch.url="http://elasticsearch:9200"
    - server.name="kibana"

    # Health check to confirm availability of Kibana
    healthcheck:
      # test: ["CMD", "curl", "-s", "-f", "-u", "${elastic_user}:${elastic_password}", "http://localhost:${port}/login"]
      test: ["CMD", "curl", "-s", "-f", "http://localhost:${port}/login"]
      retries: 6

    # We don't start Kibana until the ES instance is ready
    depends_on:
    - elasticsearch

    # Internal network for the containers
    $(networks ${networks})
EOF
