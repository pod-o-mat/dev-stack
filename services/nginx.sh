#!/bin/sh -e
source ./services/common.sh

name='nginx'
port=80
exposed_port=8100
networks='edge dev'
unsecure=true
cat <<EOF

  #  NGINX
  ###############################################################################
  ${name}:
    image: registry.gitlab.com/pod-o-mat/docker/nginx:latest
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}")
    $(labels ${name} ${port} ${unsecure})
    volumes:
    - ${config}/nginx/conf.d:/etc/nginx/conf.d
    - ${config}/nginx/passwords:/etc/nginx/passwords

EOF
