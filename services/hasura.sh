#!/bin/sh -e
source ./services/common.sh

name='hasura'
port=8080
exposed_port=8088
networks='edge database'

cat <<EOF
#  Hasura
###############################################################################
  ${name}:
    image: hasura/graphql-engine:v1.1.0
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}")
    $(labels ${name})
    environment:
    - HASURA_GRAPHQL_DATABASE_URL=postgres://${hasura_postgres_user}:${hasura_postgres_password}@postgres:5432/${hasura_db}
    - HASURA_GRAPHQL_ENABLE_CONSOLE=true
    - HASURA_GRAPHQL_ENABLED_LOG_TYPES=startup, http-log, webhook-log, websocket-log, query-log

    depends_on:
    - postgres
    - hasura_configure_postgres

    healthcheck:
      test: /bin/wget -q --spider http://127.0.0.1:${port}/console

    $(networks ${networks})
EOF

name='hasura_configure_postgres'
networks=''database''
cat <<EOF
#  hasura_configure_postgres
###############################################################################

  # Configure Stack container. This short lived container configures the stack once Kibana and Elasticsearch
  # are available. More specifically, using a script it sets passwords, import dashboards,
  # sets a default index pattern, loads templates and pipelines
  ${name}:
    image: postgres:12
    restart: always
    hostname: ${name}
    $(container_name ${name})
    environment:
    - PGUSER=${postgres_user}
    - PGPASSWORD=${postgres_password}
    volumes:
    - ${config}/hasura/sql:/app/sql:ro
    command: /bin/sh -c 'until psql -h postgres -d ${postgres_db} -c "\q"; do \
                           printf "." ; \
                           sleep 1 ; \
                         done ; \
                         echo "" ; \
                         psql -h postgres -d ${postgres_db} -a \
                              -f /app/sql/hasura-0001.sql ; \
                         while [ true ] ; do sleep 3600 ; done'
    depends_on:
    - postgres
    $(networks ${networks})
EOF