#!/bin/sh -e
source ./services/common.sh

name='auditbeat'
networks='edge elk iot database hadoop metric dev'

cat <<EOF

  #  auditbeat
  ###############################################################################
  ${name}:
    image: docker.elastic.co/beats/auditbeat:${elastic_version}
    restart: always
    hostname: ${name}
    $(container_name ${name})

    environment:
    - ES_HOST=elasticsearch:9200

    volumes:
    - ${config}/elastic/auditbeat.yml:/usr/share/auditbeat/auditbeat.yml:ro

    user: root
    pid: host
    cap_add:
    - AUDIT_CONTROL
    - AUDIT_READ

    command: auditbeat --strict.perms=false -e
    $(networks ${networks})
EOF
