#!/bin/sh -e
source ./services/common.sh

name='kafka'
port=9092
exposed_port=9092
networks='hadoop'
cat <<EOF
#  Kafka
###############################################################################

  ${name}:
    image: registry.gitlab.com/pod-o-mat/bigdata/kafka
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}")
    environment:
    - ZOOKEEPER_PORT=2181
    - ZOOKEEPER_HOST=zookeeper
    healthcheck:
      test: /bin/bash -c '[[ \$\$(/opt/kafka/bin/zookeeper-shell.sh \$\${ZOOKEEPER_HOST}:\$\${ZOOKEEPER_PORT} <<< "ls /brokers/ids" | tail -1  | grep "^\[\]") != "[]" ]]'
      # timeout: 5s
      # interval: 15s
      # retries: 4

    depends_on:
    - zookeeper
    $(networks ${networks})
EOF
