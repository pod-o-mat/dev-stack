#!/bin/sh -e
source ./services/common.sh

name='mlflow'
port=5000
exposed_port=5000
networks='edge hadoop'
cat <<EOF
  #  mlflow
  ###############################################################################
  ${name}:
    image: registry.gitlab.com/pod-o-mat/bigdata/mlflow:v1.7.2-003
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}")
    $(labels ${name})

    volumes: $(volumes ${name} 'mlruns:/opt/mlruns')
    environment:
    $([ "${minio}"  == "true" ] && echo "- AWS_ACCESS_KEY_ID=${s3_access_key_id}")
    $([ "${minio}"  == "true" ] && echo "- AWS_SECRET_ACCESS_KEY=${s3_secret_access_key}")
    $([ "${minio}"  == "true" ] && echo "- MLFLOW_S3_ENDPOINT_URL=http://minio:9000")
    $([ "${minio}"  == "true" ] && echo "- S3_USE_SIGV4=True")
    $([ "${minio}"  == "true" ] && echo "- AWS_DEFAULT_REGION=us-east-1")

    healthcheck:
      test: /usr/bin/wget -q --spider http://127.0.0.1:${port}

    depends_on:
    $([ "${minio}"  == "true" ] && echo "- minio")

    networks:
    - edge
    - hadoop

EOF

