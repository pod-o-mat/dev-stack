#!/bin/sh -e
source ./services/common.sh

name='theia'
port=3000
exposed_port=4000
networks='edge dev'
cat <<EOF
  #  THEIA - Web IDE
  ###############################################################################
  ${name}:
    image: registry.gitlab.com/pod-o-mat/docker/theia:v0.16.1-002
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}")
    $(labels 'ide')
    volumes: $(workspace ${name} '/home/project')
    - /var/run/docker.sock:/var/run/docker.sock

    healthcheck:
      test: /usr/bin/wget -q --spider http://localhost:${port}

    $(networks ${networks})
EOF
