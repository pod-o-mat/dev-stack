#!/bin/sh -e
source ./services/common.sh

name='mongodb'
port=27017
exposed_port=27017
networks='database'
cat <<EOF
  #  MongoDB
  ###############################################################################

  ${name}:
    hostname: ${name}
    image: mongo:3.6-xenial
    restart: always
    $(container_name ${name})
    $(ports "${exposed_port}:${port}")
    volumes: $(volumes ${name} 'data:/data/db')
    environment:
    - MONGO_INITDB_ROOT_USERNAME=${mongo_username}
    - MONGO_INITDB_ROOT_PASSWORD=${mongo_password}
    healthcheck:
      test: echo 'db.runCommand("ping").ok' | mongo 127.0.0.1:${port}/test --quiet

    $(networks ${networks})
EOF

name='mongo'
port=8081
exposed_port=8081
networks='edge database'
cat <<EOF
  #  Mongo EXPRESS
  ###############################################################################

  ${name}:
    image: mongo-express:0.54
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}")
    $(labels ${name})
    environment:
    - ME_CONFIG_OPTIONS_EDITORTHEME=ambiance
    - ME_CONFIG_MONGODB_SERVER=mongodb
    - ME_CONFIG_MONGODB_ADMINUSERNAME=${mongo_username}
    - ME_CONFIG_MONGODB_ADMINPASSWORD=${mongo_password}
    # - ME_CONFIG_BASICAUTH_USERNAME=${mongo_express_username}
    # - ME_CONFIG_BASICAUTH_PASSWORD=${mongo_express_password}

    healthcheck:
      test: /usr/bin/wget -q --spider http://127.0.0.1:${port}
    depends_on:
    - mongodb

    $(networks ${networks})
EOF
