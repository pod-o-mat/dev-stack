#!/bin/sh -e
source ./services/common.sh

name='minio'
port=9000
exposed_port=9000
networks='edge hadoop'
cat <<EOF
#  minio
###############################################################################
  ${name}:
    image: minio/minio:RELEASE.2020-04-04T05-39-31Z
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}")
    $(labels ${name} ${port})
    volumes:$(volumes ${name} data:/data)
    command: server /data

    environment:
    - MINIO_ACCESS_KEY=${s3_access_key_id}
    - MINIO_SECRET_KEY=${s3_secret_access_key}

    healthcheck:
      test: /usr/bin/wget -q --spider http://127.0.0.1:${port}/minio/health/live

    $(networks ${networks})
EOF
