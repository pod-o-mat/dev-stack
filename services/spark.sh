#!/bin/sh -e
source ./services/common.sh

name='spark'
networks='hadoop edge'
port=8080
exposed_port=8080


cat <<EOF
  #  HBase
  ###############################################################################
  ${name}:
    image: registry.gitlab.com/pod-o-mat/bigdata/spark:v2.4.5-0001
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(labels ${name} ${port})
    $(ports "${exposed_port}:${port}" "7077:7077")
    $(networks ${networks})
EOF
