#!/bin/sh -e
source ./services/common.sh

name='mqtt'
port=1883
exposed_port=1883
networks='iot'
cat <<EOF
  #  mqtt
  ###############################################################################
  ${name}:
    image: eclipse-mosquitto:1.6
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}" "9001:9001")
    volumes: $(volumes ${name} data:/mosquitto/data)
    $(networks ${networks})
EOF