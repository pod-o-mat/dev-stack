#!/bin/sh -e
source ./services/common.sh

name='apm'
port=8200
exposed_port=8200
networks='elk'

cat <<EOF
  #  apm-server
  ###############################################################################
  # ADD elastic-apm-agent-1.6.0.jar $BWCE_HOME

  # ENV JAVA_OPTS=-javaagent:$BWCE_HOME/elastic-apm-agent-1.6.0.jar -Delastic.apm.service_name=hello \
  #               -Delastic.apm.application_packages=org.example -Delastic.apm.server_url=http://apm-server:8200

  ${name}:
    image: docker.elastic.co/apm/apm-server:${elastic_version}
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}")
    environment:
    - ELASTICSEARCH_HOSTS=elasticsearch:9200

    command: -E output.elasticsearch.hosts=["elasticsearch:9200"] \
             -E apm-server.host=0.0.0.0:${port}

    depends_on:
    - elasticsearch
    $(networks ${networks})
EOF
