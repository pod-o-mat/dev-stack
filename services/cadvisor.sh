#!/bin/sh -e
source ./services/common.sh

name='cadvisor'
port=8080
exposed_port=8080
networks='metric'
cat <<EOF
  #  cadvisor
  ###############################################################################
  ${name}:
    image: google/cadvisor
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(ports "${exposed_port}:${port}")
    volumes:
    - /:/rootfs:ro
    - /var/run:/var/run:rw
    - /sys:/sys:ro
    - /var/lib/docker/:/var/lib/docker:ro
    $(networks ${networks})
EOF
