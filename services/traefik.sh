#!/bin/sh -e
source ./services/common.sh

name='traefik'
networks='edge metric elk'
cat <<EOF
  #  Traefik
  ###############################################################################
  ${name}:
    # The official v2 Traefik docker image
    image: traefik:v2.1
    restart: always
    hostname: ${name}
    $(container_name ${name})
    $(labels ${name})
    # Dashboard
    - traefik.http.routers.${name}.service=api@internal

    # Redirect http to https middleware
$(if [ "${redirect}" == "true" ] ; then
cat <<REDIRECT
    # middleware redirect
    - traefik.http.middlewares.redirect-to-https.redirectscheme.scheme=https

    # Redirect to https
    - traefik.http.routers.http-catchall.rule=hostregexp(\`{host:.+}\`)
    - traefik.http.routers.http-catchall.entrypoints=public-http
    - traefik.http.routers.http-catchall.middlewares=redirect-to-https
REDIRECT
fi)

    ports:
    $([ "${http}" == "true" ]         && echo "- 80:80")       # The HTTP port
    $([ "${https}" == "true" ]        && echo "- 443:443")     # The HTTPS port

    volumes: $(volumes 'traefik' 'logs:/var/log/traefik:rw')  # ./volumes/log/traefik:/var/log/traefik:rw
    - /var/run/docker.sock:/var/run/docker.sock
    $([ "${acme}" == "true" ] && echo "- ${HOME}/.traefik/acme.json:/acme.json")

    # Enables the web UI and tells Traefik to listen to docker
    command:
    - --api
    - --providers.docker
    - --providers.docker.exposedByDefault=false
    - --providers.docker.network=edge
    # - --providers.docker.defaultRule=Host(\`{{ .Name }}.{{ index .Labels \"customLabel\"}}\`)
    $([ "${http}"  == "true" ]  && echo "- --entryPoints.public-http.address=:80")
    $([ "${https}" == "true" ]  && echo "- --entryPoints.public-https.address=:443")

$(if [ "${acme}" == "true" -a "${staging}" == "true" ] ; then
cat <<STAGING
    - --certificatesresolvers.${resolver}.acme.caserver=https://acme-staging-v02.api.letsencrypt.org/directory
STAGING
fi)
$(if [ "${acme}" == "true" ] ; then
cat <<ACME
    - --certificatesResolvers.${resolver}.acme.email=${email}
    - --certificatesResolvers.${resolver}.acme.storage=acme.json
    # - --certificatesResolvers.${resolver}.acme.httpChallenge.entryPoint=public-http
    - --certificatesresolvers.${resolver}.acme.tlschallenge=true
ACME
fi)
    - --entryPoints.metrics.address=:9090
    - --metrics.prometheus=true
    - --metrics.prometheus.addEntryPointsLabels=true
    - --metrics.prometheus.entryPoint=metrics
    - --log.level=INFO
    - --log.filePath=/var/log/traefik/traefik.log
    - --accesslog=true
    - --accesslog.filepath=/var/log/traefik/access.log    
$(if [ "${filebeat}" == "true" ] ; then
cat <<ELK
    # Elastic logging
    - --log.format="json"

    # Elasticaudit logs
    - --accesslog.bufferingsize=100
    - --accesslog.format=json
    # - --accesslog.filters.statuscodes=200,300-302
    # - --accesslog.filters.retryattempts
    # - --accesslog.filters.minduration=10ms
    # - --accesslog.fields.defaultmode=keep
    # - --accesslog.fields.names.ClientUsername=drop
    # - --accesslog.fields.headers.defaultmode=keep
    # - --accesslog.fields.headers.names.User-Agent=redact
    # - --accesslog.fields.headers.names.Authorization=drop
    # - --accesslog.fields.headers.names.Content-Type=keep
ELK
fi)
$(if [ "${jaeger}" == "true" ] ; then
cat <<JAEGER
    # Elastic OpenTracing
    - --tracing.jaeger=true
    - --tracing.jaeger.samplingServerURL=http://jaeger:5778/sampling
    - --tracing.jaeger.samplingType=const
    - --tracing.jaeger.samplingParam=1.0
    - --tracing.jaeger.localAgentHostPort=jaeger:6831
    - --tracing.jaeger.gen128Bit
    - --tracing.jaeger.propagation=jaeger
    - --tracing.jaeger.traceContextHeaderName=uber-trace-
    - --tracing.jaeger.collector.endpoint=http://jaeger:14268/api/traces?format=jaeger.thrift
    - --tracing.jaeger.collector.user=traefik
    - --tracing.jaeger.collector.password=admin
JAEGER
fi)
    $(networks ${networks})
EOF